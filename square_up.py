#!/usr/bin/python

import os
import gtk
import time
import pickle
import random

HIGH_SCORE_FILE = os.path.expanduser("~/.square_up_highscores")

num_tiles_in_row = 5
blank_tile_color = "#232323"
colors_for_default_grid = [
    "#0AD42C",
    "#FF8909",
    "#FFFF00",
    "#800080",
    "#F95E79",
    "#7BBAE3"
]
left_key = gtk.keysyms.Left
right_key = gtk.keysyms.Right
up_key = gtk.keysyms.Up
down_key = gtk.keysyms.Down

class Tile(gtk.EventBox):
    def __init__(self, x, y, value, colors):
        gtk.EventBox.__init__(self)
        self.ev = gtk.EventBox()
        self.ev.show()
        self.add(self.ev)
        self.ev.set_size_request(20, 20)
        self.ev.set_border_width(2)
        self.x = x
        self.y = y
        self.value = value
        self.colors = colors
        self.is_correct = False
        self.update()
    def set_value(self, value):
        self.value = value
        self.update()
    def set_colors(self, colors):
        self.colors = colors
        self.update()

    def update(self):
        color = self.colors[self.value]
        self.ev.modify_bg(gtk.STATE_NORMAL, self.colors[self.value])
        self.correct(self.is_correct)

    def correct(self, it_is):
        self.is_correct = it_is
        color = self.colors[self.value]
        values = color.hue, color.saturation, color.value

        if it_is:
            values = (1-h for h in values) 

        self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_from_hsv(*values))

class Game(gtk.Window):
    def __init__(self, num_tiles_in_row, blank_color,
                 left_key, right_key, up_key, down_key,
                 colors_for_default_grid):
        gtk.Window.__init__(self)
        self.connect("window-state-event", self.on_fullscreen_event)
        self.is_fullscreen = False
        self.colors_for_default_grid = colors_for_default_grid
        self.default_num_tiles_in_row = num_tiles_in_row
        self.set_title("Square Up")
        self.set_icon_name("squareup")
        self.blank_color = blank_color
        self.left_key = left_key
        self.right_key = right_key
        self.up_key = up_key
        self.down_key = down_key
        self.vbox = gtk.VBox()
        self.vbox.set_spacing(5)
        self.add(self.vbox)

        menu_items = (
            ("/_Game", None, None, 0, "<Branch>"),
            ("/Game/_New Game", "<control>N", lambda *args: self.new_game(), 0, "<StockItem>", gtk.STOCK_NEW),
            ("/Game/New _Grid", "<control>G", self.new_grid_window, 0, "<StockItem>", gtk.STOCK_NEW),
            ("/Game/_Fullscreen", "F11", lambda widget, event: self.toggle_fullscreen(), 0, "<StockItem>", gtk.STOCK_FULLSCREEN),
            ("/Game/", None, None, 0, "<Separator>"),
            ("/Game/_Quit", "<control>Q", gtk.main_quit, 0, "<StockItem>", gtk.STOCK_QUIT),
            ("/_Help", None, None, 0, "<Branch>"),
            ("/Help/_About", "F1", self.about_window, 0, "<StockItem>", gtk.STOCK_ABOUT)
        )
        accel_group = gtk.AccelGroup()
        item_factory = gtk.ItemFactory(gtk.MenuBar, "<main>", accel_group)
        item_factory.create_items(menu_items)
        self.add_accel_group(accel_group)
        menubar = item_factory.get_widget("<main>")
        item_factory.get_item("/Game/New Game").set_tooltip_text("New Game")
        item_factory.get_item("/Game/New Grid").set_tooltip_text("Change the size and/or the colors of your grid")
        item_factory.get_item("/Game/Fullscreen").set_tooltip_text("Toggle fullscreen view")
        item_factory.get_item("/Help/About").set_tooltip_text("About squareup")
        self.item_factory = item_factory
        self.alignment = gtk.Alignment()
        self.alignment.add(menubar)
        self.vbox.pack_start(self.alignment, False, False)

        self.hbox = gtk.HBox()
        self.highscore_text = "<b>Highscore:</b>\n\t%(moves)d moves\n\t%(seconds)d seconds"
        self.highscore_label = gtk.Label(self.highscore_text % {'moves': 0, 'seconds': 0})
        self.highscore_label.set_use_markup(True)
        self.score_text = "<b>Score:</b>\n\t%(moves)d moves\n\t%(seconds)d seconds"
        self.score_label = gtk.Label(self.score_text % {'moves': 0, 'seconds': 0})
        self.score_label.set_use_markup(True)
        self.hbox.pack_start(self.highscore_label, False)
        self.hbox.pack_start(self.score_label, False)
        self.todo_table = gtk.Table()
        self.todo_table.set_row_spacings(2)
        self.todo_table.set_col_spacings(2)
        self.hbox.pack_end(self.todo_table, False)
        self.vbox.pack_start(self.hbox, False)
        self.play_eventbox = gtk.EventBox()
        self.play_eventbox.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(blank_color))
        self.play_table = gtk.Table()
        self.play_table.set_row_spacings(5)
        self.play_table.set_col_spacings(5)
        self.play_eventbox.add(self.play_table)
        self.play_table.show()
        self.vbox.add(self.play_eventbox)
        self.connect("key-press-event", self.on_key)
        self.highscores = {}
        self.new_grid(num_tiles_in_row)
        self.new_game()
        self.figure_score()
        gtk.timeout_add(200, self.update_score_label)

    def new_grid(self, num_tiles_in_row):
        self.num_tiles_in_row = num_tiles_in_row
        for child in self.play_table.get_children():
            self.play_table.remove(child)
        for child in self.todo_table.get_children():
            self.todo_table.remove(child)
        self.play_table.resize(num_tiles_in_row, num_tiles_in_row)
        if num_tiles_in_row == self.default_num_tiles_in_row:
            self.set_colors(self.colors_for_default_grid)
        else:
            self.set_colors()
        rows = []
        tiles = []
        for row in range(num_tiles_in_row):
            rows.append([])
            for column in range(num_tiles_in_row):
                tile = Tile(column, row, 0, self.colors)
                rows[-1].append(tile)
                tiles.append(tile)
                self.play_table.attach(tile, column, column + 1, row, row + 1)
                tile.show()
        self.rows = rows
        self.tiles = tiles


    def set_colors(self, default=None):
        if default is not None:
            self.colors = [gtk.gdk.Color(color) for color in [self.blank_color] + default]
            return
        number = self.num_tiles_in_row + 1
        multiplier = 1. / (number + 1)
        start_out_list = list(((x + 1) * multiplier) for x in range(number))
        hues = start_out_list[:]
        random.shuffle(hues)
        values = start_out_list[:]
        random.shuffle(values)
        saturations = [(num/2.)+0.5 for num in start_out_list]
        random.shuffle(saturations)
        self.colors = [gtk.gdk.Color(self.blank_color)] + list(gtk.gdk.Color("#" + "".join((hex(int(n * 255))[2:]).zfill(2) for n in gtk.hsv_to_rgb(*l))) for l in zip(hues, values, saturations))


    def new_game(self):
        self.turns = 0
        self.start_time = None
        self.beat_highscore = False
        for row in self.rows:
            for tile in row:
                if tile.x == 0 and tile.y == len(self.rows) - 1:
                    tile.set_value(0)
                else:
                    tile.set_value((tile.x + 1 if (tile.y < len(self.rows) - 1) else len(self.colors) - 1))
        self.blank_position = (0, len(self.rows) - 1)

        t_rows = []
        t_tiles = []
        numbers = []
        numbers_available = range(1, len(self.colors))
        num_around_the_edge = self.num_tiles_in_row // 4
        if num_around_the_edge == 0:
            num_around_the_edge = 1
        for row in range(self.num_tiles_in_row - (num_around_the_edge * 2)):
            t_rows.append([])
            for column in range(self.num_tiles_in_row - (num_around_the_edge * 2)):
                t_num = random.choice(numbers_available)
                numbers.append(t_num)
                if numbers.count(t_num) >= self.num_tiles_in_row - 1:
                    numbers_available.remove(t_num)
                tile = Tile(column, row, t_num, self.colors)
                t_rows[-1].append(tile)
                t_tiles.append(tile)
                self.todo_table.attach(tile, column, column + 1, row, row + 1)
        self.t_rows = t_rows
        self.t_tiles = t_tiles
        self.todo_table.show_all()
        self.over = False
        if self.check_game_over():
            self.new_game()

    def left(self):
        if self.blank_position[0] == len(self.rows[0]) - 1:
            return
        x, y = self.blank_position
        self.rows[y][x].set_value(self.rows[y][x + 1].value)
        x += 1
        self.rows[y][x].set_value(0)
        self.blank_position = (x, y)
        if self.turns == 0:
            self.start_time = time.time()
        self.turns += 1
        if self.check_game_over():
            self.on_game_over()

    def right(self):
        if self.blank_position[0] == 0:
            return
        x, y = self.blank_position
        self.rows[y][x].set_value(self.rows[y][x - 1].value)
        x -= 1
        self.rows[y][x].set_value(0)
        self.blank_position = (x, y)
        if self.turns == 0:
            self.start_time = time.time()
        self.turns += 1
        if self.check_game_over():
            self.on_game_over()

    def up(self):
        if self.blank_position[1] == len(self.rows) - 1:
            return
        x, y = self.blank_position
        self.rows[y][x].set_value(self.rows[y + 1][x].value)
        y += 1
        self.rows[y][x].set_value(0)
        self.blank_position = (x, y)
        if self.turns == 0:
            self.start_time = time.time()
        self.turns += 1
        if self.check_game_over():
            self.on_game_over()

    def down(self):
        if self.blank_position[1] == 0:
            return
        x, y = self.blank_position
        self.rows[y][x].set_value(self.rows[y - 1][x].value)
        y -= 1
        self.rows[y][x].set_value(0)
        self.blank_position = (x, y)
        if self.turns == 0:
            self.start_time = time.time()
        self.turns += 1
        if self.check_game_over():
            self.on_game_over()


    def on_key(self, widget, event):
        if event.keyval == self.left_key:
            self.left()
        elif event.keyval == self.right_key:
            self.right()
        elif event.keyval == self.up_key:
            self.up()
        elif event.keyval == self.down_key:
            self.down()
        elif event.keyval == gtk.keysyms.r and not ((event.state - 4) & (event.state - 5)):
            self.set_colors()
            for tile in self.tiles:
                tile.colors = self.colors
                tile.update()
            for tile in self.t_tiles:
                tile.colors = self.colors
                tile.update()
        elif event.keyval == gtk.keysyms.Escape and self.is_fullscreen:
            self.toggle_fullscreen()

    def check_game_over(self):
        num_around_the_edge = self.num_tiles_in_row // 4
        if num_around_the_edge == 0:
            num_around_the_edge = 1
        over = True
        for tile in self.t_tiles:
            other_tile = self.rows[tile.y + num_around_the_edge][tile.x + num_around_the_edge]
            if other_tile.value == tile.value:
                other_tile.correct(True)
            else:
                other_tile.correct(False)
                over = False
        if not over:
            self.figure_score()
        return over

    def on_game_over(self):
        self.update_score_label()
        self.over = True
        self.check_highscore()
        self.celebrate()
        self.new_game()

    def check_highscore(self):
        score = self.figure_score()
        if score[0] > self.highscores.get(self.num_tiles_in_row, (0,))[0]:
            self.beat_highscore = True
            self.highscore_label.set_markup(self.highscore_text % {
                'moves': score[1],
                'seconds': round(score[2])
            })
            self.highscores[self.num_tiles_in_row] = score
            with open(HIGH_SCORE_FILE, 'w') as pickle_file:
                pickler = pickle.Pickler(pickle_file)
                pickler.dump(self.highscores)


    def figure_score(self):
        try:
            previous = self.highscores.get(self.num_tiles_in_row, (0, 0, 0))
            unpickler = pickle.Unpickler(open(HIGH_SCORE_FILE))
            self.highscores = unpickler.load()
            now = self.highscores.get(self.num_tiles_in_row, (0, 0, 0))
            self.highscore_label.set_markup(self.highscore_text % {
                'moves': now[1],
                'seconds': round(now[2])
            })
        except (IOError, pickle.PickleError):
            pass
        return (((self.turns == 0) and -1) or (1. / (self.turns * (time.time() - self.start_time))), self.turns, (-1 if self.turns == 0 else time.time() - self.start_time))

    def update_score_label(self):
        if self.over: return True
        if self.start_time is None:
            self.score_label.set_markup(self.score_text % {
                'moves': 0,
                'seconds': 0,
            })
            return True
        self.score_label.set_markup(self.score_text % {
            'moves': self.turns, 'seconds': time.time() - self.start_time
        })
        return True

    def celebrate(self):
        md = gtk.Dialog()
        md.set_title("Victory!")
        md.set_modal(True)
        md.set_transient_for(self)
        moves = self.turns
        if self.start_time is None:
            take_time = 0
        else:
            take_time = time.time() - self.start_time
        minutes, seconds = divmod(take_time, 60)
        if minutes >= 60:
            take_time = "%.02d:%.02d:%.02d" % (divmod(minutes, 60) + (seconds,))
        else:
            take_time = "%.02d:%.02d" % (minutes, round(seconds))
        message = "You won!\nYou did it with %d moves in %s" % (moves, take_time)
        if self.beat_highscore:
            message += "\nYou beat the highscore!"
        label = gtk.Label(message)
        label.set_justify(gtk.JUSTIFY_CENTER)
        md.vbox.add(label)
        md.vbox.show_all()
        md.run()
        md.destroy()

    def new_grid_window(self, *args):
        if self.start_time is None:
            time_passed = None
        else:
            time_passed = time.time() - self.start_time
            self.start_time = None
        class ColorButtonList(list):
            def __init__(self, vbox, colors):
                list.__init__(self)
                self.vbox = vbox
                for i in range(len(colors) - 1):
                    self.append(gtk.ColorButton(colors[i + 1]))
                    self.vbox.pack_start(self[-1], False)
                    self[-1].show()
            def add(self):
                self.append(gtk.ColorButton())
                self.vbox.pack_start(self[-1], False)
                self[-1].show()
            def remove(self):
                self.vbox.remove(self.pop())
            def update(self, number):
                num_colors = len(self)
                if number > num_colors:
                    for _ in range(number - num_colors):
                        self.add()
                elif number < num_colors:
                    for _ in range(num_colors - number):
                        self.remove()
            def get_colors(self):
                return list(button.get_color().to_string() for button in self)
        window = gtk.Dialog()
        window.set_title("Change Grid")
        window.set_transient_for(self)
        window.set_modal(True)
        window.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OK, gtk.RESPONSE_OK)
        vbox = window.get_content_area()
        grid_label = gtk.Label()
        grid_label.set_text_with_mnemonic("Grid _Colors:")
        grid_button = gtk.SpinButton(
            gtk.Adjustment(self.num_tiles_in_row + 1, 4, 15, 1, 3),
            digits=0
        )
        grid_label.set_mnemonic_widget(grid_button)
        grid_button.set_numeric(True)
        vbox.pack_start(grid_label, False)
        vbox.pack_start(grid_button, False)
        color_button_list = ColorButtonList(vbox, self.colors)
        grid_button.connect("value-changed", lambda widget: color_button_list.update(widget.get_value_as_int()))
        vbox.show_all()
        response = window.run()
        window.destroy()
        if response == gtk.RESPONSE_OK:
            num_colors = grid_button.get_value_as_int() - 1
            if num_colors != self.num_tiles_in_row:
                self.new_grid(num_colors)
                self.new_game()
            self.set_colors(color_button_list.get_colors())
            for tile in self.tiles:
                tile.colors = self.colors
                tile.update()
            for tile in self.t_tiles:
                tile.colors = self.colors
                tile.update()
        if time_passed is not None:
            self.start_time = time.time() - time_passed

    def about_window(self, *args):
        about = gtk.AboutDialog()
        about.set_skip_taskbar_hint(True)
        about.set_program_name("Square Up")
        about.set_website("https://github.com/ralphembree/squareup")
        about.set_authors(("Ralph Embree",))
        icon = gtk.icon_theme_get_default().load_icon("vim", 80, ())
        about.set_logo(icon)
        about.set_comments("""The object of the game is to copy the small pattern into the middle area of the big board.  Arrow keys are used for movement.  (hint: If you are familiar with PyGTK, you can change which keys are used by editing the source file of this game.  At the beginning of the file, they are set.  Change them to how you like them.)""")
        about.set_license(
"""SquareUp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

SquareUp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with SquareUp.  If not, see http://www.gnu.org/licenses""")
        about.run()
        about.destroy()

    def toggle_fullscreen(self, *args):
        if self.is_fullscreen:
            self.unfullscreen()
        else:
            self.fullscreen()

    def on_fullscreen_event(self, widget, event):
        if event.changed_mask & gtk.gdk.WINDOW_STATE_FULLSCREEN:
            self.is_fullscreen = event.new_window_state & gtk.gdk.WINDOW_STATE_FULLSCREEN

if __name__ == '__main__':
    game = Game(num_tiles_in_row, blank_tile_color,
                left_key, right_key, up_key, down_key,
                colors_for_default_grid)
    game.connect('destroy', gtk.main_quit)
    game.show_all()
    gtk.mainloop()
